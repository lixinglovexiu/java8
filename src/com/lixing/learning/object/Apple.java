package com.lixing.learning.object;

import java.math.BigDecimal;

public class Apple {
    private Integer id;
    private String type;
    private BigDecimal price;
    private Integer num;

    public Apple() {
    }

    public Apple(Integer id, String type, BigDecimal price, Integer num) {
        this.id = id;
        this.type = type;
        this.price = price;
        this.num = num;
    }

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getNum() {
        return num;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
