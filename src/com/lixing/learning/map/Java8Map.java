package com.lixing.learning.map;

import com.lixing.learning.object.Apple;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Java8Map {
    public static void main(String[] args) {
        // 构建list
        Map<Integer, Apple> apples = new HashMap();
        Apple hongfushi = new Apple(1, "红富士", new BigDecimal("3.2"), 300);
        Apple hongxing = new Apple(2, "红星", new BigDecimal("2.6"), 200);
        Apple sheguo = new Apple(3, "蛇果", new BigDecimal("10.4"), 400);
        Apple tianshui = new Apple(4, "天水", new BigDecimal("5.4"), 400);
        apples.put(hongfushi.getId(), hongfushi);
        apples.put(hongxing.getId(), hongxing);
        apples.put(sheguo.getId(), sheguo);
        apples.put(tianshui.getId(), tianshui);

        /*遍历key和value*/
        // 通过keySet
        apples.keySet().forEach(key -> System.out.println("key: " + key + " value: " + apples.get(key)));
        // 通过entrySet
        apples.entrySet().iterator().forEachRemaining(item -> System.out.println("key: " + item.getKey() + " value: " + item.getValue()));
        // 数据量大时推荐使用下面的方式
        apples.entrySet().forEach(item -> System.out.println("key: " + item.getKey() + " value: " + item.getValue()));
        // 遍历valueSet
        apples.values().forEach(System.out::println);
        // 通过k，v遍历
        apples.forEach((k,v) -> System.out.println("key: " + k + " value: " + v));
    }
}
