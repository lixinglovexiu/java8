package com.lixing.learning;

import com.lixing.learning.object.Apple;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * java8增加了optional类来处理null
 */
public class Java8Optional {

    public static void main(String[] args) {


    }

    public String getAppleType(Apple apple) {
        return Optional.ofNullable(apple).map(apple1 -> apple1.getType()).orElse("");
    }

    public String getAppleTypeOrDefault(Apple apple) {
        return Optional.ofNullable(apple).map(apple1 -> apple1.getType()).orElseGet(() -> getDefaultType());
        //        return Optional.ofNullable(apple).map(apple1 -> apple1.getType()).orElse(getDefaultType());
    }

    private String getDefaultType() {
        return "烂苹果";
    }

    /**
     * ifPresent的用法
     */
    public void soutAppleType(Apple apple) {
        Optional<Apple> appleOpt = Optional.ofNullable(apple);
        appleOpt.ifPresent(apple1 -> System.out.println(apple1.getType()));
    }

    /**
     * 连续非空判断
     */
    public void continuousNullJudge(Apple apple) {
        System.out.println(Optional.ofNullable(apple)
                .map(apple1 -> apple1.getType())
                .map(type -> type.toUpperCase())
                .orElse(""));
    }

    /**
     * 校验参数合法性
     */
    public void setPrice(String price) throws Exception {
        Apple apple = new Apple();
        String checkedPrice = Optional.ofNullable(price)
                .filter(Java8Optional::priceIsValid)
                .orElseThrow(() -> new IllegalArgumentException("invalid param price"));

        apple.setPrice(new BigDecimal(checkedPrice));
    }

    private static boolean priceIsValid(String price) {
        try {
            Double.parseDouble(price);
        }catch (Exception e) {
            return false;
        }
        return true;
    }
}
