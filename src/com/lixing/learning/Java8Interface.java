package com.lixing.learning;

public interface Java8Interface {
    // 可以定义默认方法
    default String defaultMethod() {
        return "default method";
    }

    // 可以定义静态方法
    static String staticMethod() {
        return "static method";
    }
}
