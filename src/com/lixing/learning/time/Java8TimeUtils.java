package com.lixing.learning.time;

import java.time.*;
import java.time.temporal.TemporalAdjusters;

public class Java8TimeUtils {
    public static void main(String[] args) {
        // 判断是否是闰年
        LocalDate today = LocalDate.now();
        boolean isLeapYear = today.isLeapYear();

        // 判断是否在指定日期之前/之后
        boolean isBefore = today.isBefore(LocalDate.of(2019, 4,20));
        boolean isAfter = today.isAfter(LocalDate.of(2019, 4,20));

        // LocalDate -> LocalDateTime
        LocalDateTime now = today.atTime(LocalTime.now());

        // 加减
        LocalDate date1 = today.plusDays(10);
        LocalDate date2 = today.plusWeeks(10);
        LocalDate date3 = today.plusMonths(10);
        LocalDate date4 = today.plusYears(10);
        LocalDate date5 = today.minusYears(10);

        // 本月/本年第一天
        LocalDate firstDayOfMonth = today.with(TemporalAdjusters.firstDayOfMonth());
        LocalDate firstDayOfYear = today.with(TemporalAdjusters.firstDayOfYear());

        Period period = firstDayOfYear.until(today);
        period.getDays();
        period.getMonths();
    }
}

