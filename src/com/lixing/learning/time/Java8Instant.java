package com.lixing.learning.time;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * java.time.Instant使用在机器可读的时间格式上的。
 * 它以Unix时间戳的形式存储日期时间。
 */
public class Java8Instant {
    public static void main(String[] args) {
        Instant now = Instant.now();
        Instant specificTime = Instant.ofEpochSecond(10000);
        Duration thirtyDay = Duration.ofDays(30);
    }
}

