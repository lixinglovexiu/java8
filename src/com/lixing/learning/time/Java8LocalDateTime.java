package com.lixing.learning.time;

import java.time.*;

public class Java8LocalDateTime {
    public static void main(String[] args) {
        // 当前时间 2014-04-28T16:00:49.455
        LocalDateTime now = LocalDateTime.now();
        // 当前时间
        LocalDateTime now1 = LocalDateTime.of(LocalDate.now(), LocalTime.now());
        // 指定时间
        LocalDateTime specificDateTime = LocalDateTime.of(2017, 12, 20, 12, 24, 32);
        // 指定时区
        LocalDateTime nowTokyo = LocalDateTime.now(ZoneId.of("Asia/Tokyo"));
        // 1970-01-01后的指定秒数
        LocalDateTime dateFromBase = LocalDateTime.ofEpochSecond(10000, 0, ZoneOffset.UTC);
    }
}

