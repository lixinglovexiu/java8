package com.lixing.learning.time;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;

public class Java8LocalDate {
    public static void main(String[] args) {
        // 当前时间 yyyy-MM-dd
        LocalDate today = LocalDate.now();
        // 指定时间 2014-01-01
        LocalDate day20140101 = LocalDate.of(2014, Month.JANUARY, 1);
        // 指定时区
        LocalDate todayTokyo = LocalDate.now(ZoneId.of("Asia/Tokyo"));
        // 1970-01-01后的指定天数
        LocalDate dataFromBase = LocalDate.ofEpochDay(300);
        // 指定年份后的指定天数
        LocalDate hundredDayAfter2014 = LocalDate.ofYearDay(2014, 100);
    }
}

