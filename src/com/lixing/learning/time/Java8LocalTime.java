package com.lixing.learning.time;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;

public class Java8LocalTime {
    public static void main(String[] args) {
        // 当前时间 时：分：秒. 毫秒
        LocalTime now = LocalTime.now();
        // 指定时间 11:30:24.124
        LocalTime specificTime = LocalTime.of(11, 30, 24, 124*1000000);
        // 指定时区
        LocalTime nowTokyo = LocalTime.now(ZoneId.of("Asia/Tokyo"));
        // 指定秒数 12:00:01 不带毫秒
        LocalTime specificSecondTime = LocalTime.ofSecondOfDay(12*60*60+1);
    }
}

