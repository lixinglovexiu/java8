package com.lixing.learning.list;

import com.lixing.learning.object.Apple;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Java8List {
    public static void main(String[] args) {
        // 构建list
        List<Apple> apples = new ArrayList<>();
        Apple hongfushi = new Apple(1, "红富士", new BigDecimal("3.2"), 300);
        Apple hongxing = new Apple(1, "红星", new BigDecimal("2.6"), 200);
        Apple sheguo = new Apple(3, "蛇果", new BigDecimal("10.4"), 400);
        Apple tianshui = new Apple(4, "天水", new BigDecimal("5.4"), 400);
        apples.add(hongfushi);
        apples.add(hongxing);
        apples.add(sheguo);
        apples.add(tianshui);

        //max min average sum
        apples.stream().mapToInt(Apple::getNum).max();
        apples.stream().mapToInt(Apple::getNum).min();
        apples.stream().mapToInt(Apple::getNum).average();
        apples.stream().mapToInt(Apple::getNum).sum();

        // map reduce
        BigDecimal totalMoney = apples.stream().map(Apple::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);

        // list to map
        /** 需要注意的是：
         * toMap 如果集合对象有重复的key，会报错Duplicate key ....
         *  例如apple1,apple12的id都为1。
         *  可以用 (k1,k2)->k1 来设置，如果有重复的key,则保留key1,舍弃key2
         *  */
        Map<Integer, Apple> appleMap = apples.stream().collect(Collectors.toMap(Apple::getId, apple -> apple, (k1, k2) -> k1));

        // 分组
        Map<Integer, List<Apple>> group = apples.stream().collect(Collectors.groupingBy(Apple::getId));

        // 过滤 price > 3.0
        /**
         * bigDecimal 比较大小使用 compareTo 方法
         * 返回结果==-1表示小于
         * 返回结果==0表示等于
         * 返回结果==1表示大于
         */
        List<Apple> filterList = apples.stream().filter(apple -> apple.getPrice().compareTo(new BigDecimal(3.0)) == 1).collect(Collectors.toList());

        // 排序
        Comparator<Apple> comparator = Comparator.comparing(Apple::getPrice);
        Comparator<Apple> comparatorDesc = new Comparator<Apple>() {
            @Override
            public int compare(Apple o1, Apple o2) {
                return o2.getPrice().compareTo(o1.getPrice());
            }
        };
        apples.sort(comparator);
        System.out.println(apples.stream().map(Apple::getPrice).collect(Collectors.toList()));
        apples.sort(comparatorDesc);
        System.out.println(apples.stream().map(Apple::getPrice).collect(Collectors.toList()));

    }
}
